const express = require("express");
const cors = require("cors");

const app = express();

app.use(express.json());
app.use(cors());

require("./src/database");

// Require and use product routes
const ProductRoutes = require("./src/routes/products");

app.use(ProductRoutes);
// Status Endpoint
app.get("/", (req, res) => res.send({ success: "App Started Successfully" }));

module.exports = app;
