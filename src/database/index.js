const mongoose = require("mongoose");

const { MONGODB_URL } = require("../utils/constants").ENVIRONMENTS;

mongoose.connect(
  MONGODB_URL,
  {
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true,
  },
  (err) => {
    if (err) throw err;
    console.log("Connected to DB");
  }
);
