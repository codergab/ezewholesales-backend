const successResponse = (message = "Operation Successful", data = []) => {
  return {
    status: true,
    message,
    data,
  };
};

const errorResponse = (message = "It has broken again :(", data) => {
  return {
    status: false,
    message,
    data,
  };
};

module.exports = {
  successResponse,
  errorResponse,
};
