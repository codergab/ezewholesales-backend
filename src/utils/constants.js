const HTTP_STATUS_CODES = {
  SUCCESS: 200,
  CREATED: 201,
  BAD_REQUEST: 400,
  SERVER_ERROR: 500,
};

const REQUESTS = {
  RESULTS_PER_PAGE: 20,
  DEFAULT_PAGE_NUMBER: 1,
};

const ENVIRONMENTS = {
  PORT: process.env.PORT || 5000,
  MONGODB_URL:
    process.env.MONGODB_URI || "mongodb://localhost:27017/eze-wholesales",
};

module.exports = {
  HTTP_STATUS_CODES,
  ENVIRONMENTS,
  REQUESTS,
};
