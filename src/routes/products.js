const express = require("express");
const router = express.Router();

const ProductController = require("../controller/ProductController");

/**
 * Get Products
 * Buy Requests & Sell Requests
 */
router.get("/products/:page?", ProductController.getBuyAndSellRequestProducts);

/**
 * POST Products
 * Buy Requests || && Sell Requests
 */
router.post("/products", ProductController.storeBuySellRequests);

/**
 * Filter Products
 */
router.get("/products/filter/query", ProductController.filterProducts);

module.exports = router;
