const Mongoose = require("mongoose");
const Schema = Mongoose.Schema;

const buyRequestSchema = new Schema({
  phone_manufacturer: {
    type: String,
    default: "Generic",
  },
  phone_model: {
    type: String,
    default: "",
  },
  phone_price: {
    type: Number,
    default: 0,
  },
  phone_storage_capacity: {
    type: String,
    default: "64GB",
  },
  phone_condition: {
    type: String,
    default: "A1",
  },
  phone_status: {
    type: String,
    default: "Unlocked",
  },
});

const BuyRequest = Mongoose.model("BuyRequest", buyRequestSchema);

module.exports = BuyRequest;
