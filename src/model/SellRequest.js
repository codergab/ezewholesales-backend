const Mongoose = require("mongoose");
const Schema = Mongoose.Schema;

const sellRequestSchema = new Schema({
  phone_manufacturer: {
    type: String,
    default: "Generic",
  },
  phone_model: {
    type: String,
    default: "",
  },
  phone_price: {
    type: Number,
    default: 0,
  },
  phone_storage_capacity: {
    type: String,
    default: "32GB",
  },
  phone_condition: {
    type: String,
    default: "A1",
  },
  phone_status: {
    type: String,
    default: "Unlocked",
  },
  in_stock: {
    type: Number,
    default: 1,
  },
});

const SellRequest = Mongoose.model("SellRequest", sellRequestSchema);

module.exports = SellRequest;
