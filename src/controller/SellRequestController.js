const SellRequest = require("../model/SellRequest");

class SellRequestController {
  // GET Sell Request Products
  async getSellRequestProducts({ resultsPerPage, pageNumber }) {
    const sellRequests = await SellRequest.find().sort({ createdAt: -1 });
    // .skip(resultsPerPage * pageNumber - 1)
    // .limit(resultsPerPage);

    const totalSellRequests = await SellRequest.countDocuments();

    return {
      data: sellRequests,
      pages: Math.ceil(totalSellRequests / resultsPerPage),
      currentPage: pageNumber,
    };
  }

  // Store Requests
  async storeSellRequests(payload) {
    const sellRequest = new SellRequest({
      ...payload,
      phone_storage_capacity: payload.phone_storage_capacity.toUpperCase(),
    });

    try {
      await sellRequest.save();
      return sellRequest;
    } catch (error) {
      throw error;
    }
  }
}

const sellRequestController = new SellRequestController();

module.exports = sellRequestController;
