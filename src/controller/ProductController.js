const BuyRequestController = require("./BuyRequestController");
const SellRequestController = require("./SellRequestController");
const SellRequests = require("../model/SellRequest");

const { successResponse } = require("../utils/responseHandler");
const {
  RESULTS_PER_PAGE,
  DEFAULT_PAGE_NUMBER,
} = require("../utils/constants").REQUESTS;

class ProductController {
  // Get BuyRequests and Sell Requests Products
  async getBuyAndSellRequestProducts(req, res) {
    // Filters Criteria
    const resultsPerPage = RESULTS_PER_PAGE;
    const pageNumber = req.params.page || DEFAULT_PAGE_NUMBER;

    const criteria = {
      resultsPerPage,
      pageNumber,
    };

    let buyRequests = await BuyRequestController.getBuyRequestProducts(
      criteria
    );
    let sellRequests = await SellRequestController.getSellRequestProducts(
      criteria
    );

    res.send(
      successResponse("Products Fetched", {
        buyRequests,
        sellRequests,
      })
    );
  }

  // Post BuyRequests || && Sell Requests
  async storeBuySellRequests(req, res) {
    const shouldSell = req.body.shouldSell;
    const shouldBuy = req.body.shouldBuy;

    const payload = req.body;

    if (shouldBuy) {
      await BuyRequestController.storeBuyRequests(payload);
    }

    if (shouldSell) {
      await SellRequestController.storeSellRequests(payload);
    }

    res.send(successResponse("Products Saved"));
  }

  // Filter Products
  async filterProducts(req, res) {
    const match = {};

    const price = {
      min: "",
      max: "",
    };

    if (req.query.price) {
      price.min = req.query.price.split(",")[0];
      price.max = req.query.price.split(",")[1];
    }

    if (req.query.storage) {
      match.phone_storage_capacity = req.query.storage.split(",");
    }

    const matches = await SellRequests.find({ match });
    res.send(matches);
  }
}

const productController = new ProductController();

module.exports = productController;
