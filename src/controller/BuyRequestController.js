const BuyRequest = require("../model/BuyRequest");

class BuyRequestController {
  // GET Buy Request Products
  async getBuyRequestProducts({ resultsPerPage, pageNumber }) {
    const buyRequests = await BuyRequest.find().sort({ createdAt: -1 });
    // .skip(resultsPerPage * pageNumber - 1)
    // .limit(resultsPerPage);

    const totalBuyRequests = this.getTotalBuyRequests();

    return {
      data: buyRequests,
      pages: Math.ceil(totalBuyRequests / resultsPerPage),
      currentPage: pageNumber,
    };
  }

  // Store Requests
  async storeBuyRequests(payload) {
    const buyRequest = new BuyRequest({
      ...payload,
      phone_storage_capacity: payload.phone_storage_capacity.toUpperCase(),
    });

    try {
      await buyRequest.save();
      return buyRequest;
    } catch (error) {
      throw error;
    }
  }

  async getTotalBuyRequests() {
    return await BuyRequest.countDocuments();
  }
}

const buyRequestController = new BuyRequestController();

module.exports = buyRequestController;
