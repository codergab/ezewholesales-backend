const app = require("./app");

const { PORT } = require("./src/utils/constants").ENVIRONMENTS;

app.listen(PORT, () => {
  console.log(
    `App Started and Running\nVisit 127.0.0.1:${PORT} on your browser`
  );
});
